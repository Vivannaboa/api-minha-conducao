var app = require('./src/config/app_config.js');
var db = require('./src/config/db_config');
var validator = require('validator');
var usuarios = require('./src/routes/usuarioRouter');
var empresa = require('./src/routes/empresaRouter');
var horario = require('./src/routes/horarioRouter');
var instituicao = require('./src/routes/instituicaoRouter');
var motorista = require('./src/routes/motoristaRouter');
var passageiro = require('./src/routes/passageiroRouter');
var roteiro = require('./src/routes/roteiroRouter');
var veiculo = require('./src/routes/veiculoRouter');

app.get('/', function(req, res) {
    res.end('Bem-vindo a API Minha Condução');
});

//Rotas de usuario
app.use('/usuario', usuarios);

//Rotas de ncm
app.use('/horario', horario);

//Rotas de instituicao
app.use('/instituicao', instituicao);

//Rotas de motorista
app.use('/motorista', motorista);

//Rotas de passageiro
app.use('/passageiro', passageiro);

//Rotas de roteiro
app.use('/roteiro', roteiro);

//Rotas de veiculo
app.use('/veiculo', veiculo);

//Rotas de empresa
app.use('/empresa', empresa);
