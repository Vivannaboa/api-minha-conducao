'use strict';
var express = require('express');
var router = express.Router();
var MotoristaController = require('../controller/motoristaController');
var UsuarioController = require('../controller/usuarioController');
var validator = require('validator');


function pegarToken(req, res, next) {
    var header = req.headers['authorization'];

    if (typeof header !== 'undefined') {
        req.token = header;
        next();
    } else {
        res.sendStatus(403);
    }
}

router.get('/token', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            MotoristaController.getToken(token, function (resp) {
                if (resp) {
                    res.json(resp);
                } else {
                    res.status(409).send(resp);
                }
            });
        }
        else {
            res.sendStatus(403);
        }
    });
});



router.post('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var nome = req.body.nome;
            var cpf = req.body.cpf;
            var cnh = req.body.cnh;
            var telefone = req.body.telefone;
            var token_organizacao = resp;
            var lat = req.body.lat;
            var lng = req.body.lng;

            MotoristaController.save(nome, cpf, cnh, telefone, lat, lng, token, token_organizacao, function (resp) {
                if (resp.error) {
                    res.status(412).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });
});

router.put('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp === true) {
            var nome = req.body.id;
            var nome = req.body.nomes;
            var cpf = req.body.cpf;
            var cnh = req.body.cnh;
            var telefone = req.body.telefone;
            var lat = req.body.lat;
            var lng = req.body.lng;
            MotoristaController.update(id, nome, cpf, cnh, telefone, lat, lng, function (resp) {
                if (resp.error) {
                    res.status(409).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });
});

router.put('/atualizaLatLng', function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var lat = req.body.latAtual;
            var lng = req.body.lngAtual;
            var id = req.body.id;
            MotoristaController.atualizaLatLng(id, lat, lng, function (resp) {
                if (resp.error) {
                    res.status(409).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    })
});

module.exports = router;
