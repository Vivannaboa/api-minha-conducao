'use strict';
var express = require('express');
var router = express.Router();
var PassageiroController = require('../controller/passageiroController');
var UsuarioController = require('../controller/usuarioController');
var validator = require('validator');

function pegarToken(req, res, next) {
    var header = req.headers['authorization'];
    if (typeof header !== 'undefined') {
        req.token = header;
        next();
    } else {
        res.sendStatus(403);
    }
}

router.get('/token', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            PassageiroController.getToken(token, function (resp) {
                if (resp) {
                    res.json(resp);
                } else {
                    res.status(409).send(resp);
                }
            }
            );
        }
        else {
            res.sendStatus(403);
        }
    });
});

router.get('/id', pegarToken, function (req, res) {
    var token = req.token;
    var id = req.headers.uid;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            PassageiroController.getId(id, function (resp) {
                if (resp) {
                    res.json(resp);
                } else {
                    res.status(409).send(resp);
                }
            }
            );
        }
        else {
            res.sendStatus(403);
        }
    });
});

router.get('/', pegarToken, function (req, res) {
    var token = req.token;

    function listaPassageiro(token_organizacao) {
        return new Promise((resolve, reject) => { // (A)
            PassageiroController.list(token_organizacao, function (resp) {
                resolve(resp);
            }
            );
        });
    }
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            let token_organizacao = resp;
            listaPassageiro(token_organizacao)
                .then(
                x => PassageiroController.getCount(token_organizacao, function (resp) {
                    if (resp) {
                        res.header('Total-Registros-Restantes', resp);
                    }
                    else {
                        res.header('Total-Registros-Restantes', 0);
                    }
                    res.json(x);
                })
                );
        }
        else {
            res.sendStatus(403);
        }
    });
});

router.post('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var nome = req.body.nome;
            var cpf = req.body.cpf;
            var logradouro = req.body.logradouro;
            var numero = req.body.numero;
            var telefone = req.body.telefone;
            var bairro = req.body.bairro;
            var cidade = req.body.cidade;
            var lat = req.body.lat;
            var lng = req.body.lng;
            var token_organizacao = resp;


            PassageiroController.save(nome, cpf, logradouro, numero, telefone, bairro, cidade, lat, lng, token, token_organizacao, function (resp) {
                if (resp.error) {
                    res.status(412).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }

    });

});

router.patch('/atualizaLatLng', function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var lat = req.body.lat;
            var lng = req.body.lng;
            var id = req.body.id;
            PassageiroController.atualizaLatLng(id, lat, lng, function (resp) {
                if (resp.error) {
                    res.status(409).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    })
});

router.put('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var nome = req.body.nome;
            var cpf = req.body.cpf;
            var logradouro = req.body.logradouro;
            var numero = req.body.numero;
            var telefone = req.body.telefone;
            var bairro = req.body.bairro;
            var id = req.body.id;
            var cidade = req.body.cidade;
            var lat = req.body.lat;
            var lng = req.body.lng;
            var token_organizacao = resp;
            PassageiroController.update(id, nome, cpf, logradouro, numero, telefone, bairro, cidade, lat, lng, token_organizacao, function (resp) {
                if (resp.error) {
                    res.status(409).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });

});

router.delete('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var id = validator.trim(validator.escape(req.body.id));
            PassageiroController.delete(id, function (resp) {
                res.json(resp);
            });
        } else {
            res.sendStatus(403);
        }
    });
});

module.exports = router;