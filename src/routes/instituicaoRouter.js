'use strict';
var express = require('express');
var router = express.Router();
var InstituicaoController = require('../controller/instituicaoController');
var UsuarioController = require('../controller/usuarioController');
var validator = require('validator');

function pegarToken(req, res, next) {
    var header = req.headers['authorization'];
    if (typeof header !== 'undefined') {
        req.token = header;
        next();
    } else {
        res.sendStatus(403);
    }
}

router.get('/', pegarToken, function (req, res) {
    var token = req.token;
    function listaInstituicao(token_organizacao) {
        return new Promise((resolve, reject) => { // (A)
            InstituicaoController.list(token_organizacao, function (resp) {
                resolve(resp);
            }
            );
        });
    }
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            let token_organizacao = resp;
            listaInstituicao(token_organizacao)
                .then(
                x => InstituicaoController.getCount(token_organizacao, function (resp) {
                    if (resp) {
                        res.header('Total-Registros-Restantes', resp);
                    }
                    else {
                        res.header('Total-Registros-Restantes', 0);
                    }
                    res.json(x);
                })
                );
        }
        else {
            res.sendStatus(403);
        }
    });
});

router.post('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {

            var razao_social = req.body.razao_social;
            var cnpj = req.body.cnpj;
            var logradouro = req.body.logradouro;
            var numero = req.body.numero;
            var email = req.body.email;
            var telefone = req.body.telefone;
            var bairro = req.body.bairro;
            var cidade = req.body.cidade;
            var token_organizacao = resp;
            var lng = req.body.lng;
            var lat = req.body.lat;
            InstituicaoController.save(razao_social, cnpj, logradouro, numero, email, telefone, bairro, cidade,lat,lng, token_organizacao, function (resp, error) {
                if (resp.error) {
                    res.status(412);
                    res.json(resp.error);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.status(403);
        }
    });
});

router.put('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var razao_social = req.body.razao_social;
            var cnpj = req.body.cnpj;
            var logradouro = req.body.logradouro;
            var numero = req.body.numero;
            var email = req.body.email;
            var telefone = req.body.telefone;
            var bairro = req.body.bairro;
            var cidade = req.body.cidade;
            var token_organizacao = resp;
            var lng = req.body.lng;
            var lat = req.body.lat;
            InstituicaoController.update(id, razao_social, cnpj, logradouro, numero, email, telefone, bairro, cidade,lat,lng, token_organizacao, function (resp) {
                if (resp.error) {
                    res.status(409).send(resp);
                } else {
                    res.json(resp);
                }
                });
        } else {
            res.sendStatus(403);
        }
    });
});

router.delete('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var id = validator.trim(validator.escape(req.body.id));
            InstituicaoController.delete(id, function (resp) {
                res.json(resp);
            });
        } else {
            res.sendStatus(403);
        }
    });
});

module.exports = router;