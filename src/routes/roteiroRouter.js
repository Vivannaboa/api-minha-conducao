'use strict';
var express = require('express');
var router = express.Router();
var RoteiroController = require('../controller/roteiroController');
var UsuarioController = require('../controller/usuarioController');
var validator = require('validator');


function pegarToken(req, res, next) {
    var header = req.headers['authorization'];

    if (typeof header !== 'undefined') {
        req.token = header;
        next();
    } else {
        res.sendStatus(403);
    }
}

router.get('/', pegarToken, function (req, res) {
    var token = req.token;
    function listaRoteiro(token_organizacao) {
        return new Promise((resolve, reject) => { // (A)
            RoteiroController.list(token_organizacao, function (resp) {
                resolve(resp);
            }
            );
        });
    }
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            let token_organizacao = resp;
            listaRoteiro(token_organizacao)
                .then(
                x => RoteiroController.getCount(token_organizacao, function (resp) {
                    if (resp) {
                        res.header('Total-Registros-Restantes', resp);
                    }
                    else {
                        res.header('Total-Registros-Restantes', 0);
                    }
                    res.json(x);
                })
                );
        }
        else {
            res.sendStatus(403);
        }
    });
});


router.post('/addPassageiro', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {   
            
            var id = req.body.id;
            var id_passageiro = req.body.id_passageiro;
             
            RoteiroController.addPassageiro(id, id_passageiro, function (resp) {
                if (resp.error) {
                    res.status(412).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });

});


router.post('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {   
            
            var descricao = req.body.descricao;
            var horaInicio = req.body.horaInicio;
            var horaFim = req.body.horaFim;
            var latInicio = req.body.latInicio;
            var latFim = req.body.latFim;
            var lngInicio = req.body.lngInicio;
            var lngFim = req.body.lngFim;                        
            var token_organizacao = resp;
    
            RoteiroController.save(descricao, horaInicio, horaFim, latInicio, lngInicio, latFim, lngInicio,lngFim,token_organizacao, function (resp) {
                if (resp.error) {
                    res.status(412).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });

});

router.put('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var descricao = req.body.descricao;
            var horaInicio = req.body.horaInicio;
            var horaFim = req.body.horaFim;
            var latInicio = req.body.latInicio;
            var latFim = req.body.latFim;
            var lngInicio = req.body.lngInicio;
            var lngFim = req.body.lngFim;                        
            var token_organizacao = resp;
    
            RoteiroController.update(id,descricao, horaInicio, horaFim, latInicio, lngInicio, latFim, lngFim, function (resp) {
                if (resp.error) {
                    res.status(409).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });

});

router.delete('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp === true) {
            var id = req.body.id;
            RoteiroController.delete(id,  function (resp) {
                res.json(resp);
            });
        } else {
            res.sendStatus(403);
        }
    });

});

module.exports = router;
