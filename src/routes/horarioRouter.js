'use strict';
var express = require('express');
var router = express.Router();
var HorarioController = require('../controller/horarioController');
var usuarioController = require('../controller/usuarioController');
var validator = require('validator');

function pegarToken(req, res, next) {
    var header = req.headers['authorization'];
    if (typeof header !== 'undefined') {
        req.token = header;
        next();
    } else {
        res.sendStatus(403);
    }
}

router.get('/', pegarToken, function (req, res) {
    var token = req.token;
    function listaHorario(token_organizacao) {
        return new Promise((resolve, reject) => { // (A)
            HorarioController.list(token_organizacao, function (resp) {
                resolve(resp);
            }
            );
        });
    }
    usuarioController.authorize(token, function (resp) {
        if (resp != false) {
            let token_organizacao = resp;
            listaHorario(token_organizacao)
                .then(
                x => HorarioController.getCount(token_organizacao, function (resp) {
                    if (resp) {
                        res.header('Total-Registros-Restantes', resp);
                    }
                    else {
                        res.header('Total-Registros-Restantes', 0);
                    }
                    res.json(x);
                })
                );
        }
        else {
            res.sendStatus(403);
        }
    });
});

router.post('/id', pegarToken, function (req, res) {
    var token = req.token;
    let id = req.body.id_passageiro;
    usuarioController.authorize(token, function (resp) {
        if (resp != false) {
            let token_organizacao = resp;
            HorarioController.getById(token_organizacao, id, function (resp, error) {
                if (resp) {
                    res.json(resp);
                } else {
                    res.json(error);
                }
            })

        }
        else {
            res.sendStatus(403);
        }
    });
});


router.post('/', pegarToken, function (req, res) {
    var token = req.token;
    usuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var ativo = req.body.ativo;
            var dia = req.body.dia;
            var hora = req.body.hora;
            var uid_instituicao = req.body.uid_instituicao;
            var uid_passageiro = req.body.uid_passageiro;
            var token_organizacao = resp;

            HorarioController.save(dia, hora, uid_instituicao, uid_passageiro, token_organizacao,ativo, function (resp, error) {
                if (resp.error) {
                    res.status(412);
                    res.json(resp.error);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });
});

router.put('/', pegarToken, function (req, res) {
    var token = req.token;
    usuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var id = req.body.id;
            var dia = req.body.dia;
            var ativo = req.body.ativo;
            var hora = req.body.hora;
            var uid_instituicao = req.body.uid_instituicao;
            var uid_passageiro = req.body.uid_passageiro;

            HorarioController.update(id,dia, hora, uid_instituicao, uid_passageiro, ativo, function (resp) {
                if (resp.error) {
                    res.status(409).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });

});

router.delete('/', pegarToken, function (req, res) {
    var token = req.token;
    usuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var id;
            var data_exclusao;
            if (req.headers.id) {
                id = validator.trim(validator.escape(req.headers.id));
                data_exclusao = req.headers.data_exclusao;
            }
            if (!id) {
                id = validator.trim(validator.escape(req.body.id));
                data_exclusao = req.body.data_exclusao;
            }

            HorarioController.delete(id, data_exclusao, token, function (resp) {
                if (resp.error) {
                    res.status(412).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });
});
module.exports = router;