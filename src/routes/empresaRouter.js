'use strict';
var express = require('express');
var router = express.Router();
var empresaController = require('../controller/empresaController');
var usuarioController = require('../controller/usuarioController');
var validator = require('validator');

function pegarToken(req, res, next) {
    var header = req.headers['authorization'];
    if (typeof header !== 'undefined') {
        req.token = header;
        next();
    } else {
        res.sendStatus(403);
    }
}

router.get('/', pegarToken, function (req, res) {
    var token = req.token;
    function listaEmpresas(token_organizacao) {
        return new Promise((resolve, reject) => {
            empresaController.list(token_organizacao, function (resp) {
                resolve(resp);
            }
            );
        });
    }

    usuarioController.authorize(token, function (resp) {
        if (resp != false) {
            listaEmpresas()
                .then(
                x => empresaController.getCount(resp, function (resp) {
                    if (resp) {
                        res.header('Total-Registros-Restantes', resp);
                    }
                    else {
                        res.header('Total-Registros-Restantes', 0);
                    }
                    res.json(x);
                })
                );
        } else {
            res.sendStatus(403);
        }
    });
});

router.post('/', pegarToken, function (req, res) {
    var token = req.token;
    usuarioController.authorize(token, function (resp) {
        if (resp != false) {
            let razao_social = req.body.razao_social;
            let cnpj = req.body.cnpj;
            let logradouro = req.body.logradouro;
            let numero = req.body.numero;
            let email = req.body.email;
            let telefone = req.body.telefone;
            let bairro = req.body.bairro;
            let cidade = req.body.cidade;
            empresaController.save(razao_social, cnpj, logradouro, numero, email, telefone, bairro, cidade, function (resp) {
                if (resp.error) {
                    res.status(412);
                    res.json(resp.error);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });
});

router.put('/', pegarToken, function (req, res) {
    var token = req.token;
    usuarioController.authorize(token, function (resp) {
        if (resp != false) {
            let razao_social = req.body.razao_social;
            let cnpj = req.body.cnpj;
            let logradouro = req.body.logradouro;
            let numero = req.body.numero;
            let email = req.body.email;
            let telefone = req.body.telefone;
            let bairro = req.body.bairro;
            let cidade = req.body.cidade;
            empresaController.update(id, razao_social, nome_fantasia, cnpj, logradouro, numero, email, telefone, bairro, cidade, function (resp) {
                if (resp.error) {
                    res.status(412);
                    res.json(resp.error);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });
});

router.delete('/', pegarToken, function (req, res) {
    var token = req.token;
    usuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var id = req.body.id;
            empresaController.delete(id, resp, function (resp) {
                res.json(resp);
            });
        } else {
            res.sendStatus(403);
        }
    });

});
module.exports = router;