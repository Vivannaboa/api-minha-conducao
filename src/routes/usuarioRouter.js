var express = require('express');
var router = express.Router();
var validator = require('validator');
var emailValidator = require("email-validator");

var usuarioController = require('../controller/usuarioController');

function pegarToken(req, res, next) {
    var header = req.headers['authorization'];

    if (typeof header !== 'undefined') {
        req.token = header;
        next();
    } else {
        res.sendStatus(403);
    }
}

router.post('/cadastrar', function (req, res) {
    var tipo = req.body.tipo;
    var senha = req.body.senha;
    var email = req.body.email;
    var dispositivo = req.body.dispositivo;
    var token_organizacao = req.body.token_organizacao;
    if (emailValidator.validate(email)) {
        usuarioController.save(tipo, senha, email, token_organizacao, function (resp, error) {
            if (error) {
                if (error == "Duplicidade") {
                    res.status(409);
                    res.json(error.message);
                } else {
                    res.status(422);
                    res.json(error.message);
                }
            } else {
                res.json(resp);
            }
        })
    } else {
        res.status(422);
        res.json("Email inválido");
    }
});

router.post('/configurar', function (req, res) {
    var email = req.body.email;
    var senha = req.body.senha;
    usuarioController.configurar(email, senha, function (resp, error) {
        if (error) {
            res.status(401);
            res.json(error.message);
        } else {
            res.json(resp);
        }
    })

});

router.post('/login', function (req, res) {
    var email = req.body.email;
    var senha = req.body.senha;
    usuarioController.login(email, senha, function (resp, error) {
        if (error) {
            res.status(401);
            res.json(error.message);
        } else {
            res.json(resp);
        }

    })
});

router.get('/listar', pegarToken, function (req, res) {
    var token = req.token;
    usuarioController.list(token, function (resp) {
        res.json(resp);
    })
});

router.put('/atualizar', pegarToken, function (req, resposta) {
    try {
        var token = req.token;
        var tipo = req.body.tipo;
        var senha = req.body.senha;
        var token_organizacao = req.body.token_organizacao;
        usuarioController.getid(token, function (res, req) {
            let id = res;
            console.log(id);
            usuarioController.update(id, tipo, senha, token_organizacao, function (resp) {
                console.log(resp);
                resposta.json(resp);
            });
        });
    } catch (e) {
        resposta.json(e.message);
    }
});

module.exports = router;