'use strict';
var express = require('express');
var router = express.Router();
var VeiculoController = require('../controller/veiculoController');
var UsuarioController = require('../controller/usuarioController');
var validator = require('validator');

function pegarToken(req, res, next) {
    var header = req.headers['authorization'];
    if (typeof header !== 'undefined') {
        req.token = header;
        next();
    } else {
        res.sendStatus(403);
    }
}

router.get('/', pegarToken, function (req, res) {
    var token = req.token;
    function listaveiculo(token_organizacao) {
        return new Promise((resolve, reject) => { // (A)
            VeiculoController.list(token_organizacao, function (resp) {
                resolve(resp);
            }
            );
        });
    }
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            let token_organizacao = resp;
            listaveiculo(token_organizacao)
                .then(
                x => VeiculoController.getCount(token_organizacao, function (resp) {
                    if (resp) {
                        res.header('Total-Registros-Restantes', resp);
                    }
                    else {
                        res.header('Total-Registros-Restantes', 0);
                    }
                    res.json(x);
                })
                );
        }
        else {
            res.sendStatus(403);
        }
    });
});

router.post('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {

            var placa = req.body.placa;
            var modelo = req.body.modelo;
            var capacidade = req.body.capacidade;
            var token_organizacao = resp;


            VeiculoController.save(placa,modelo,capacidade,token_organizacao, function (resp) {
                if (resp.error) {
                    res.status(412).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
        
    });

});

router.put('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var placa = req.body.placa;
            var modelo = req.body.modelo;
            var capacidade = req.body.capacidade;
            var token_organizacao = resp;

            VeiculoController.update(id, placa, modelo, capacidade, function (resp) {
                if (resp.error) {
                    res.status(409).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });

});

router.delete('/', pegarToken, function (req, res) {
    var token = req.token;
    UsuarioController.authorize(token, function (resp) {
        if (resp != false) {
            var id;
            var data_exclusao;
            if (req.headers.id){
                id = validator.trim(validator.escape(req.headers.id));
                data_exclusao = req.headers.data_exclusao;
            }
            if (!id){
                id = validator.trim(validator.escape(req.body.id));
                data_exclusao = req.body.data_exclusao;
            }    

            VeiculoController.delete(id, data_exclusao, function (resp) {
                if (resp.error) {
                    res.status(412).send(resp);
                } else {
                    res.json(resp);
                }
            });
        } else {
            res.sendStatus(403);
        }
    });
});


module.exports = router;