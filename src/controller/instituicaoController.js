var Instituicao = require('../models/instituicao');

exports.save = function (razao_social, cnpj, logradouro, numero, email, telefone, bairro, cidade, lat, lng, token_organizacao, callback) {
    let _instituicao = new Instituicao();
    _instituicao.razao_social = razao_social;
    _instituicao.cnpj = cnpj;
    _instituicao.logradouro = logradouro;
    _instituicao.numero = numero;
    _instituicao.email = email;
    _instituicao.telefone = telefone;
    _instituicao.bairro = bairro;
    _instituicao.cidade = cidade;
    _instituicao.token_organizacao = token_organizacao;
    _instituicao.lat = lat;
    _instituicao.lng = lng;
    _instituicao.save(function (error, empresa) {
        if (error) {
            callback({ error });
        } else {
            callback(empresa);
        }
    });
}

exports.list = function (token, callback) {
    var query = { 'token_organizacao': token };

    Instituicao.find(query).limit(500).exec((err, instituicao) => {
        if (err) {
            callback({ error: 'Não foi possível listar intituicao' });
        } else {
            callback(instituicao);
        }
    });
}

exports.getCount = function (token, callback) {
    var query = { 'token_organizacao': token };
    Instituicao.count(query).exec((err, instituicao) => {
        callback(instituicao);
    });
}

exports.getId = function (id, callback) {
    Instituicao.find(id, function (error, instituicao) {
        if (error) {
            callback({ error: 'Não foi possível encontrar intituicao' });
        } else {
            callback(instituicao);
        }
    });
}

exports.update = function (id, razao_social, cnpj, logradouro, numero, email, telefone, bairro, cidade, token_organizacao, callback) {
    Instituicao.findById(id, function (error, _instituicao) {
        if (error || _instituicao == null) {
            callback({ error: 'ID intituicao não informado ou não encontrado' });
        } else {
            if (razao_social) {
                _instituicao.razao_social = razao_social;
            }
            if (cnpj) {
                _instituicao.cnpj = cnpj;
            }
            if (logradouro) {
                _instituicao.logradouro = logradouro;
            }
            if (numero) {
                _instituicao.numero = numero;
            }
            if (email) {
                _instituicao.email = email;
            }
            if (telefone) {
                _instituicao.telefone = telefone;
            }
            if (bairro) {
                _instituicao.bairro = bairro;
            }
            if (cidade) {
                _instituicao.cidade = cidade;
            }
            if (lat) {
                _instituicao.lat = lat;
            }
            if (lng) {
                _instituicao.lng = lng;
            }
            _instituicao.save(function (error, instituicao) {
                if (error) {
                    callback({ error: 'Não foi possível salvar intituicao' })
                } else {
                    callback(instituicao);
                }
            });
        }
    });
};
