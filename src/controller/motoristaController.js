var Motorista = require('../models/motorista');

exports.save = function (nome, cpf, cnh, telefone,lat,lng,token,token_organizacao, callback) {

    let _motorista = new Motorista();
    _motorista.nome = nome;
    _motorista.cpf = cpf;
    _motorista.cnh = cnh;
    _motorista.telefone = telefone;
    _motorista.token_organizacao = token_organizacao;
    _motorista.lat = lat;
    _motorista.lng = lng;
    _motorista.token = token;
    
    _motorista.save(function (error, motorista) {
        if (error) {
            callback({ error });
        } else {
            callback(motorista);
        }
    });
}

exports.list = function (token_organizacao, callback) {
    var query = { 'token_organizacao': token_organizacao }; //aqui é o filtro para pegar somente os registros que o usuário ainda não tem usuário

    Motorista.find(query).limit(500).exec((err, motoristas) => {
        if (err) {
            callback({ error: 'Não foi possível listar o motorista' });
        } else {
            callback(motoristas);
        }
    });
}

exports.getCount = function (token_organizacao, callback) {
    var query = { 'token_organizacao': token_organizacao }; //aqui é o filtro para pegar somente os registros que o usuário ainda não tem usuário
    Motorista.count(query).exec((err, motorista) => {
        callback(motorista);
    });
}

exports.getId = function (id, callback) {
    Motorista.find(id, function (error, motorista) {
        if (error) {
            callback({ error: 'Não foi possível encontrar o motorista' });
        } else {
            callback(motorista);
        }
    });
}

exports.getToken = function (token, callback) {
    Motorista.find({"token":token}, function (error, motorista) {
        if (error) {
            callback({ error: 'Não foi possível encontrar o motorista' });
        } else {
            callback(motorista);
        }
    });
}
exports.delete = function (id, callback) {
    Motorista.findById(id, function (error, motorista) {
        if (error) {
            callback({ error: 'Não foi possivel excluir!' });
        } else {
            motorista.excluido = true;
            motorista.data_exclusao = new Date;
            motorista.save(function (error, motorista) {
                if (error) {
                    callback({ error: 'Não foi possível excluir o motorista' })
                } else {
                    callback({ resposta: 'Subgrupo produto excluído com sucesso' });
                }
            });
        }
    });
}

exports.update = function (id, nome, cpf, cnh, telefone,lat,lng, callback) {
    Motorista.findById(id, function (error, _motorista) {
        if (error || _motorista == null) {
            callback({ error: 'ID o motorista não informado ou não encontrado' });
        } else {
            if (nome) {
                _motorista.nome = nome;
            }
            if (cpf) {
                _motorista.cpf = cpf;
            }
            if (cnh) {
                _motorista.cnf = cnh;
            }
            if (telefone) {
                _motorista.telefone = telefone;
            }
            if (lat) {
                _motorista.lat = lat;
            }
            if (lng) {
                _motorista.lng =  lng;
            }
            _motorista.save(function (error, motorista) {
                if (error) {
                    callback({ error: 'Não foi possível salvar o motorista' })
                } else {
                    callback(motorista);
                }
            });
        }
    });
};

exports.atualizaLatLng = function(id, latAtual, lngAtual,callback){
    Motorista.findById(id, function (error, _motorista) {
        if (error || _motorista == null) {
            callback({ error: 'ID o motorista não informado ou não encontrado' });
        } else {
            if (lat) {
                _motorista.latAtual = latAtual;
            }
            if (lng) {
                _motorista.lngAtual =  lngAtual;
            }
            _motorista.save(function (error, motorista) {
                if (error) {
                    callback({ error: 'Não foi possível salvar o motorista' })
                } else {
                    callback(motorista);
                }
            });
        };
    });
};