var Passageiro = require('../models/passageiro');

exports.save = function (nome, cpf, logradouro, numero, telefone, bairro, cidade, lat, lng, token,token_organizacao, callback) {
    let _passageiro = new Passageiro();
    _passageiro.nome = nome;
    _passageiro.cpf = cpf;
    _passageiro.logradouro = logradouro;
    _passageiro.numero = numero;
    _passageiro.telefone = telefone;
    _passageiro.bairro = bairro;
    _passageiro.cidade = cidade;
    _passageiro.token_organizacao = token_organizacao;
    _passageiro.lat = lat;
    _passageiro.lng = lng;
    _passageiro.token = token;

    _passageiro.save(function (error, pasageiro) {
        if (error) {
            callback({ error });
        } else {
            callback(pasageiro);
        }
    });
}

exports.list = function (token_organizacao, callback) {
    var query = { 'token_organizacao': token_organizacao }; //aqui é o filtro para pegar somente os registros que o usuário ainda não tem usuário

    Passageiro.find(query).limit(500).exec((err, pasageiro) => {
        if (err) {
            callback({ error: 'Não foi possível listar o passageiro' });
        } else {
            callback(pasageiro);
        }
    });
}

exports.getCount = function (token_organizacao, callback) {
    var query = { 'token_organizacao': token_organizacao }; //aqui é o filtro para pegar somente os registros que o usuário ainda não tem usuário
    Passageiro.count(query).exec((err, pasageiro) => {
        callback(pasageiro);
    });
}

exports.getId = function (id, callback) {
    Passageiro.findById(id, function (error, Passageiro) {
        if (error) {
            callback({ error: 'Não foi possível encontrar o passageiro' });
        } else {
            callback(Passageiro);
        }
    });
}

exports.getToken = function (token, callback) {
    Passageiro.find({"token":token}, function (error, pasageiro) {
        if (error) {
            callback({ error: 'Não foi possível encontrar o passageiro' });
        } else {
            callback(pasageiro);
        }
    });
}

exports.delete = function (id, data_exclusao, callback) {
    Passageiro.findById(id, function (error, _passageiro) {
        if (error || _passageiro === null) {
            callback({ error: 'Não foi possível excluir o passageiro' });
        } else {
            _passageiro.excluido = true;
            if (data_exclusao) {
                _passageiro.data_exclusao = data_exclusao;
            } else {
                _passageiro.data_exclusao = new Date();
            }
            _passageiro.save(function (error, pasageiro) {
                if (error) {
                    callback({ error: 'Não foi possível excluir o passageiro' })
                } else {
                    callback({ resposta: 'Tabela de preço excluído com sucesso' });
                }
            });
        }
    });
}
exports.atualizaLatLng = function(id, lat, lng,callback){
    Passageiro.findById(id, function (error, _passageiro) {
        if (error || _passageiro == null) {
            callback({ error: 'ID o passageiro não informado ou não encontrado' });
        } else {
            if (lat) {
                _passageiro.lat = lat;
            }
            if (lng) {
                _passageiro.lng = lng;
            }
            _passageiro.save(function (error, passageiro) {
                if (error) {
                    callback({ error: 'Não foi possível salvar o passageiro' })
                } else {
                    callback(passageiro);
                }
            });
        };
    });
};

exports.update = function (id, nome, cpf, logradouro, numero, telefone, bairro, cidade, lat, lng, token_organizacao, callback) {
    Passageiro.findById(id, function (error, _passageiro) {
        if (error || _passageiro == null) {
            callback({ error: 'ID o passageiro não informado ou não encontrado' });
        } else {
            if (nome) {
                _passageiro.nome = nome;
            }
            if (cpf) {
                _passageiro.cpf = cpf;
            }
            if (logradouro) {
                _passageiro.logradouro = logradouro;
            }
            if (numero) {
                _passageiro.numero = numero;
            }
            if (telefone) {
                _passageiro.telefone = telefone;
            }
            if (bairro) {
                _passageiro.bairro = bairro;
            }
            if (cidade) {
                _passageiro.cidade = cidade;
            }
            if (lat) {               
                 _passageiro.lat = lat;
            }
            if (lng) {
                _passageiro.lng = lng;
            }
                       
            _passageiro.save(function (error, passageiro) {
                if (error) {
                    callback({ error: 'Não foi possível salvar o passageiro' })
                } else {
                    callback(passageiro);
                }
            });
        }
    });
};