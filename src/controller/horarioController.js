var Horario = require('../models/horario');
var Instituicao = require('../models/instituicao');
var Passageiro = require('../models/passageiro');

function verificaInstituicaoCadastrada(uid_instituicao) {
    return new Promise((resolve, reject) => { // (A)
        Instituicao.findById(uid_instituicao, function (error, instituicaoRetorno) {
            if ((error || instituicaoRetorno == null)) {
                validacoes = validacoes + '{Instituicao não cadastrado},';
            }
            resolve(validacoes);
        });
    });
}
function verificaPassageiroCadastrado(uid_passageiro) {
    return new Promise((resolve, reject) => { // (A)
        Passageiro.findById(uid_passageiro, function (error, passageiroRetorno) {
            if ((error || passageiroRetorno == null)) {
                validacoes = validacoes + '{Passageiro não cadastrado},';
            }
            resolve(validacoes);
        });
    });
}

function validaIntegridade(uid_instituicao, uid_passageiro, callback) {
    validacoes = '';
    verificaInstituicaoCadastrada(uid_instituicao).then(x => {
        verificaPassageiroCadastrado(uid_passageiro).then(x => {
            callback({ validacoes })
        })
    });

}

exports.save = function (dia, hora, uid_instituicao, uid_passageiro, token_organizacao,ativo, callback) {
    validaIntegridade(uid_instituicao, uid_passageiro, function (retorno) {
        if (retorno.validacoes != '') {
            callback({ error: retorno });
        } else {
            let _horario = new Horario();
            _horario.dia = dia;
            _horario.ativo = ativo;
            _horario.hora = hora;
            _horario.uid_instituicao = uid_instituicao;
            _horario.uid_passageiro = uid_passageiro;
            _horario.token_organizacao = token_organizacao;
            _horario.save(function (error, horario) {
                if (error) {
                    callback({ error });
                } else {
                    callback(horario);
                }
            });
        }
    });
}

exports.list = function (token_organizacao, callback) {
    var query = { 'token_organizacao': token_organizacao }; //aqui é o filtro para pegar somente os registros que o usuário ainda não tem usuário

    Horario.find(query).limit(500).exec((err, horario) => {
        if (err) {
            callback({ error: 'Não foi possível listar horario' });
        } else {
            callback(horario);
        }
    });
}

exports.getCount = function (token_organizacao, callback) {
    var query = { 'token_organizacao': token_organizacao }; //aqui é o filtro para pegar somente os registros que o usuário ainda não tem usuário
    Horario.count(query).exec((err, horario) => {
        callback(horario);
    });
}

exports.getId = function (id, callback) {
    Horario.find(id, function (error, horario) {
        if (error) {
            callback({ error: 'Não foi possível encontrar horario' });
        } else {
            callback(horario);
        }
    });
}

exports.getById = function (token_organizacao,id, callback) {
    Horario.find({'uid_passageiro':id}, function (error, horario) {
        if (error) {
            callback({ error: 'Não foi possível encontrar horario' });
        } else {
            callback(horario);
        }
    });
}

exports.delete = function (id, data_exclusao, token, callback) {
    Horario.findById(id, function (error, _horario) {
        if (error || _horario === null) {
            callback({ error: 'Não foi possível excluir horario' });
        } else {
            _horario.excluido = true;
            if (data_exclusao) {
                _horario.data_exclusao = data_exclusao;
            } else {
                _horario.data_exclusao = new Date();
            }
            _horario.save(function (error, horario) {
                if (error) {
                    callback({ error: 'Não foi possível excluir horario' })
                } else {
                    callback({ resposta: 'horario excluído com sucesso' });
                }
            });
        }
    });
}

exports.update = function (id, dia, hora, uid_instituicao, uid_passageiro,ativo, callback) {
    validaIntegridade(uid_instituicao, uid_passageiro, function (retorno) {
        if (retorno.validacoes != '') {
            callback({ error: retorno });
        } else {
            Horario.findById(id, function (error, horario) {
                if (error || horario == null) {
                    callback({ error: 'ID horario não informado ou não encontrado' });
                } else {
                    if (dia) {
                        horario.dia = dia;
                    }
                    if (hora) {
                        horario.hora = hora;
                    }
                    if (uid_instituicao) {
                        horario.uid_instituicao = uid_instituicao;
                    }
                    if(uid_passageiro){
                        horario.uid_passageiro =uid_passageiro;
                    }

                    horario.ativo = ativo;

                    console.debug(ativo);
                    horario.save(function (error, horario) {
                        if (error) {
                            callback({ error: 'Não foi possível salvar horario' })
                        } else {
                            callback(horario);
                        }
                    });
                }
            });
        }
    });
};
