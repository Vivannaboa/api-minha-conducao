var Empresa = require('../models/empresa');
let validacoes = '';

exports.save = function (razao_social, cnpj, logradouro, numero, email, telefone, bairro, cidade,  callback) {
    let _empresa = new Empresa();
    _empresa.razao_social = razao_social;
    _empresa.cnpj = cnpj;
    _empresa.logradouro = logradouro;
    _empresa.numero = numero;
    _empresa.email = email;
    _empresa.telefone = telefone;
    _empresa.bairro = bairro;
    _empresa.cidade = cidade;
    _empresa.token_organizacao = _empresa.gerarToken(razao_social, cnpj);

    _empresa.save(function (error, empresa) {
        if (error) {
            callback({ error });
        } else {
            callback(empresa);
        }
    });
}

exports.list = function (token_organizacao, callback) {
    var query = { 'token_organizacao': token_organizacao };
    Empresa.find(query).limit(500)
        .exec((error, empresas) => {
            if (error) {
                callback({ error });
            } else {
                callback(_empresa);
            }
        });
}

exports.getCount = function (token, token_organizacao, callback) {
    var query = { 'token_organizacao': token_organizacao };
    Empresa.count(query).exec((err, _empresas) => {
        callback(_empresas);
    });
}

exports.getId = function (id, callback) {
    Empresa.find(id, function (error, _empresa) {
        if (error) {
            callback({ error: 'Não foi possível encontrar a Empresa id ' + id });
        } else {
            callback(_empresa);
        }
    });
}

exports.delete = function (id, token, callback) {
    Empresa.findById(id, function (error, _empresa) {
        if (error || _empresa == null) {
            callback({ error: 'ID empresa não informado ou não encontrado' });
        } else if (_empresa.excluido) {
            callback({ error: 'Empresa já excluída' });
        } else {
            _empresa.excluido = true;
            _empresa.data_exclusao = new Date();
            _empresa.save(function (error, empresa) {
                if (error) {
                    callback({ error: 'Não foi possível excluir EMPRESA' })
                } else {
                    callback({ resposta: 'Empresa excluída com sucesso' });
                }
            });
        }
    });
}

exports.update = function (id, razao_social, nome_fantasia, cnpj, logradouro, numero, email, telefone, bairro, cidade, callback) {
    Empresa.findById(id, function (error, _empresa) {
        if (error || _empresa == null) {
            callback({ error: "ID empresa não informado ou não encontrado" })
        }
        else {
            callback(_empresa);
        };
    });

};
