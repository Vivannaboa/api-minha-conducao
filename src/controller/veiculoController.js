var Veiculo = require('../models/veiculo');

exports.save = function ( placa,modelo,capacidade,token_organizacao, callback) {
    let _veiculo = new Veiculo();
    _veiculo.placa = placa;
    _veiculo.modelo = modelo;
    _veiculo.capacidade = capacidade;   
    _veiculo.token_organizacao = token_organizacao;
    _veiculo.save(function (error, veiculo) {
        if (error) {
            callback({ error });
        } else {
            callback(veiculo);
        }
    });
}

exports.list = function ( token_organizacao, callback) {
    var query = {'token_organizacao': token_organizacao }; //aqui é o filtro para pegar somente os registros que o usuário ainda não tem usuário

    Veiculo.find(query).limit(500).exec((err, veiculo) => {
        if (err) {
            callback({ error: 'Não foi possível listar veiculo' });
        } else {
            callback(veiculo);
        }
    });
}

exports.getCount = function (token_organizacao, callback) {
    var query = {'token_organizacao': token_organizacao }; //aqui é o filtro para pegar somente os registros que o usuário ainda não tem usuário
    Veiculo.count(query).exec((err, veiculo) => {
        callback(veiculo);
    });
}

exports.getId = function (id, callback) {
    Veiculo.find(id, function (error, veiculo) {
        if (error) {
            callback({ error: 'Não foi possível encontrar veiculo' });
        } else {
            callback(veiculo);
        }
    });
}

exports.delete = function (id, callback) {
        
}

exports.update = function (id, placa, modelo, capacidade, callback) {
    Veiculo.findById(id, function (error, _veiculo) {
        if (error || _veiculo == null) {
            callback({ error: 'ID do veiculo não informado ou não encontrado' });
        } else {  
            if (placa) {
                _veiculo.placa = placa;
            }
            if (modelo) {
                _veiculo.modelo = modelo;
            }
            if (capacidade) {
                _veiculo.capacidade = capacidade; 
            }
           _veiculo.save(function (error, veiculo) {
                if (error) {
                    callback({ error: 'Não foi possível salvar veiculo' })
                } else {
                    callback(veiculo);
                }
            });
        }
    });
};
