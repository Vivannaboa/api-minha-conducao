var Roteiro = require('../models/roteiro');
var Passageiro = require('../models/passageiro');
                        
exports.save = function (descricao, horaInicio, horaFim, latInicio, lngInicio, latFim,lngInicio, lngFim, token_organizacao, callback) {
    let _roteiro = new Roteiro();
    _roteiro.descricao = descricao;
    _roteiro.horaInicio = horaInicio;
    _roteiro.horaFim = horaFim;
    _roteiro.token_organizacao = token_organizacao;
    _roteiro.latInicio = latInicio;
    _roteiro.latFim = latFim;
    _roteiro.lngInicio = lngInicio;
    _roteiro.lngFim = lngFim;
    _roteiro.save(function (error, roteiro) {
        if (error) {
            callback({ error });
        } else {
            callback(roteiro);
        }
    });
}

exports.addPassageiro = function (id, id_passageiro, callback) {
    Passageiro.findById(id_passageiro, function (error, _passageiro) {
        if (error) {
            callback({ error: 'Não foi possível encontrar o passageiro' });
        } else {
            return _passageiro;
        }
     }).then(passageiro => {
        Roteiro.findById(id, function (error, _roteiro) {
            if (error) {
                callback({ error: 'Não foi possível excluir o roteiro' });
            } else {
                _roteiro.passageiros.push({'id': passageiro.id,'lat': passageiro.lat ,'lng': passageiro.lng});
                _roteiro.save(function (error, roteiro) {
                    if (error) {
                        callback({ error });
                    } else {
                        callback(roteiro);
                    }
                });
            }
        });
    });
}

exports.list = function (token_organizacao, callback) {
    var query = { 'token_organizacao': token_organizacao }; //aqui é o filtro para pegar somente os registros que o usuário ainda não tem usuário
    Roteiro.find(query).limit(500)
        .exec(function (error, roteiros) {
            if (error) {
                callback({ error });
            } else {
                callback(roteiros);
            }
        });
}
exports.getCount = function (token_organizacao, callback) {
    var query = { 'token_organizacao': token_organizacao }; //aqui é o filtro para pegar somente os registros que o usuário ainda não tem usuário
    Roteiro.count(query).exec((err, roteiros) => {
        callback(roteiros);
    });
}

exports.delete = function (id, callback) {
    Roteiro.findById(id, function (error, _roteiro) {
        if (error) {
            callback({ error: 'Não foi possível excluir o roteiro' });
        } else {
            _roteiro.excluido = true;
            _roteiro.data_exclusao = new Date();
            _roteiro.save(function (error) {
                if (!error) {
                    callback({ resposta: 'Roteiro excluído com sucesso' });
                }
                else {
                    callback({ error });
                }
            });
        }
    });
}

exports.update = function (id, descricao, horaInicio, horaFim, latInicio, lngInicio, latFim, lngFim, callback) {

    Roteiro.findById(id, function (error, _roteiro) {
        if (error || roteiro == null) {
            callback({ error: "ID do roteiro não informado ou não encontrado" })
        }
        else {
            if (descricao) {
                _roteiro.descricao = descricao;
            }
            if (horaInicio) {
                _roteiro.horaInicio = horaInicio;
            }
            if (horaFim) {
                _roteiro.horaFim = horaFim;
            }
            if (latInicio && lngInicio) {
                _roteiro.geoInicio = [latInicio, lngInicio];
            }
            if (latFim && lngFim) {
                _roteiro.geoFim = [latFim, lngFim]
            }
            _roteiro.save(function (error, roteiro) {
                if (error) {
                    callback({ error })
                } else {
                    callback(roteiro);
                }
            });
        }

    })

};
