var Usuario = require('../models/usuario');

exports.save = function (tipo, senha, email, token_organizacao, callback) {
    Usuario.findOne({ 'email': email })
        .exec()
        .then((usuario) => {
            if (usuario) {
                throw new Error('Duplicidade');
            }
        }).then(() => {
            var novoUsuario = new Usuario();
            novoUsuario.tipo = tipo;
            novoUsuario.email = email;
            novoUsuario.senha = novoUsuario.gerarSenha(senha);
            let novoToken = novoUsuario.gerarToken(tipo, email);
            novoUsuario.token = novoToken
            novoUsuario.token_organizacao = token_organizacao;
            novoUsuario.save(function (erro, usuario) {
                if (erro) {
                    throw new Error(erro);
                } else {
                    callback(usuario);
                }
            });
        }).catch((err) => {
            callback(err, err);
        });
}

exports.configurar = function (email, senha, callback) {
    Usuario.findOne({ 'email': email })
        .exec()
        .then((usuario) => {
            if (usuario) {
                if (usuario.validarSenha(senha)) {
                    callback(usuario);
                } else {
                    throw new Error('Senha incorreta');
                }
            } else {
                throw new Error('Usuário não existe!');
            }
        }).catch((err) => {
            callback(err, err);
        });
}

exports.login = function (email, senha, callback) {
    Usuario.findOne({ 'email': email })
        .exec()
        .then((usuario) => {
            return usuario;
        }).then((usuario) => {
            if (usuario) {
                if (usuario.validarSenha(senha)) {
                    return usuario;
                } else {
                    throw new Error('Senha incorreta');
                }
            } else {
                throw new Error('Usuário não existe!');
            }

        }).then((usuario) => {
            callback(usuario);
        }).catch((err) => {
            callback(err, err);
        });
}

exports.list = function (token, callback) {
    var query = { 'token': token }

    Usuario.findOne(query, function (erro, usuario) {
        if (erro) {
            callback(erro);
        } else if (usuario) {
            callback(usuario);
        } else {
            callback('Usuário não encontrado');
        }
    });
}
exports.authorize = function (token, callback) {
    var query = { 'token': token }
    Usuario.findOne(query, function (erro, usuario) {
        if (erro) {
            callback(false);
        } else if (usuario) {
            callback(usuario.token_organizacao);
        } else {
            callback(false);
        }
    });
}

exports.getid = function (token, callback) {
    var query = { 'token': token }
    Usuario.findOne(query, function (erro, usuario) {
        try {
            if (erro) {
                callback(false);
            } else if (usuario) {
                callback(usuario);
            } else {
                callback(0);
            }
        } catch (error) {
            callback(error.menssage)
        }
    });
}
exports.update = function (id,token_organizacao, senha, callback) {
    Usuario.findById(id, function (error, _usuario) {
        if (error || usuario == null) {
            callback({ error: 'Usuário não encontrado' });
        } else {
            if (senha) {
                _usuario.senha = senha;
            }
            if (token_organizacao) {
                _usuario.token_organizacao = token_organizacao;
            }
            _usuario.save(function (error, usuario) {
                if (error) {
                    callback({ error })
                } else {
                    callback(usuario);
                }
            });
        }
    });
};
