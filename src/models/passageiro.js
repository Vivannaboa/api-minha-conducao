var mongose = require('mongoose');
var Schema = mongose.Schema;

var PassageiroSchema = new Schema({
    nome: {
        type: String,
        message: 'O campo`{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'nome',
        path: 'nome',
        required: true
    },
    cpf: {
        type: String,
        trim: true,
        message: "O campo '{PATH}' é um campo obrigatório",       
        required: true,
        name: 'cpf',
        path: 'cpf'
    },
    logradouro: {
        type: String,
        trim: true,
        name: 'Logradouro',
        path: 'logradouro'
    },
    numero: {
        type: String,
        trim: true,
        name: 'Número',
        path: 'numero'
    },
    telefone: {
        type: String,
        trim: true,
        name: 'Telefone',
        path: 'telefone'
    },
    bairro: {
        type: String,
        trim: true,
        message: 'O `{PATH}` é um campo obrigatório!',
        required: true,
        name: "Bairro",
        path: 'bairro'
    },
    cidade: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        required: true,
        trim: true,
        name: "Cidade",
        path: 'cidade'
    },
    excluido: { type: Boolean },
    data_exclusao: {
        type: Date,
        name: 'Data de exclusão',
        path: 'data_exclusao'
    },
    token: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'descricao',
        kind: 'String',
        path: 'descricao',
        valor: String
    },
    token_organizacao: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'descricao',
        kind: 'String',
        path: 'descricao',
        valor: String
    },
    lat: {
        type: Number
      }
    ,
    lng: {
        type: Number
      }
    
}, { timestamps: true });

module.exports = mongose.model('Passageiro', PassageiroSchema,'Passageiro');

