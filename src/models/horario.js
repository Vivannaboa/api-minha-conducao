var mongose = require('mongoose');
var Schema = mongose.Schema;


var HorarioSchema = new Schema({
    dia: {
        type: String,
        message: 'O campo `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'dia',
        required: 'O campo `{PATH}` é obrigatório!',
        kind: 'String',
        path: 'dia',
        valor: String
    },
    hora: {
        type: String,
        trim: true,
        name: 'Horário',
        path: 'horario'
    },
    uid_instituicao:{
        type: Schema.Types.ObjectId, 
        ref: 'instituicao',
        message: 'A `{PATH}` é um campo obrigatório!',
        required: 'O campo `{PATH}` é obrigatório!',
        valor: Schema.Types.ObjectId,
        trim: true,
        path: 'uid_intituicao'
    },
    uid_passageiro:{
        type: Schema.Types.ObjectId, 
        ref: 'passageiro',
        message: 'A `{PATH}` é um campo obrigatório!',
        required: 'O campo `{PATH}` é obrigatório!',
        valor: Schema.Types.ObjectId,
        trim: true,
        path: 'uid_intituicao'
    },
    excluido: { type: Boolean, default: false },
    data_exclusao: {
        type: Date,
        trim: true,
        name: 'Data de exclusão',
        path: 'data_exclusao'
    },
    token_organizacao: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'descricao',
        kind: 'String',
        path: 'descricao',
        valor: String
    },
    ativo:{
        type:Boolean
    }
   
}, { timestamps: true });

module.exports = mongose.model('Horario', HorarioSchema,'Horario');