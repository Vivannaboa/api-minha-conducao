var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jsonwebtoken');
var Schema = mongoose.Schema;

var TokensSchema = new Schema({
    token: String,
    dispositivo: String
});


var minlength = [3, 'O valor do campo `{PATH}` (`{VALUE}`) não obedece  o tamanho minimo que é ({MINLENGTH}).'];

var UsuarioSchema = new Schema({
    tipo: {
        type: String,
        message: 'O tipo é um campo obrigatório!',
        trim: true,
        name: 'tipo',
        required: 'O tipo é obrigatório',
        kind: 'String',
        path: 'tipo',
        minlength: minlength,
        reason: 'Não foi informado um tipo!'
    },
    email: {
        type: String,
        trim: true,
        unique: true,
        required: 'Email é obrigatório'
         },
    senha: {
        type: String,
        trim: true,
        minlength: [8, 'O valor do campo `{PATH}` (`{VALUE}`) não obedece  o tamanho minimo que é ({MINLENGTH}).'],
    },
    token: {
        type: String,
        trim: true
    },
    token_organizacao: String
   
});

UsuarioSchema.methods.gerarToken = function (tipo, email) {
    return jwt.sign({ 'tipo': tipo, 'email': email }, 'segredomuitoForte!');
}

UsuarioSchema.methods.gerarSenha = function (senha) {
    return bcrypt.hashSync(senha, bcrypt.genSaltSync(9));
}

UsuarioSchema.methods.validarSenha = function (senha) {
    return bcrypt.compareSync(senha, this.senha);
}

module.exports = mongoose.model('Usuario', UsuarioSchema, 'Usuario');