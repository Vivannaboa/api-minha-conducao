var mongose = require('mongoose');
var Schema = mongose.Schema;

var InstituicaoSchema = new Schema({
    razao_social: {
        type: String,
        message: 'O campo`{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'Razão social',        
        path: 'razao_social',
        required: true
    },
    cnpj: {
        type: String,
        trim: true,
        message: "O campo '{PATH}' é um campo obrigatório",
        unique: true,
        required: true,
        name: 'CPF/CNPJ',
        path: 'cpf_cnpj'
    },
    logradouro: {
        type: String,
        trim: true,
        name: 'Logradouro',
        path: 'logradouro'
    },
    numero: {
        type: Number,
        trim: true,
        name: 'Número',
        path: 'numero'
    },
    email: {
        type: String,
        trim: true,
        name: 'E-mail',
        path: 'email'
    },
    telefone: {
        type: String,
        trim: true,
        name: 'Telefone',
        path: 'telefone'
    },
    bairro: {
        type: String,
        trim: true,
        message: 'O `{PATH}` é um campo obrigatório!',
        required: true,
        name: "Bairro",
        path: 'bairro'
    },
    cidade: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        required: true,
        trim: true,
        name: "Cidade",
        path: 'cidade'
    },
    excluido: { type: Boolean },
    data_exclusao: {
        type: Date,
        name: 'Data de exclusão',
        path: 'data_exclusao'
    },
    token_organizacao: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'Token da organização',
        path: 'token_organizacao'
    },
    lat: {
        type: Number
      }
    ,
    lng: {
        type: Number
      }
}, { timestamps: true });

module.exports = mongose.model('Instituicao', InstituicaoSchema,"Instituicao");