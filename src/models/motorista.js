var mongose = require('mongoose');
var Schema = mongose.Schema;

var motoristaSchema = new Schema({
    nome: {
        type: String,
        message: 'O campo `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'nome',
        required: 'O campo `{PATH}` é obrigatório!',
        kind: 'String',
        path: 'nome',
        valor: String
    },
    cpf: {
        type: String,
        message: 'o campo`{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'cpf',
        required: 'O campo `{PATH}` é obrigatório!',
        kind: 'String',
        path: 'cpf',
        valor: String
    },
    cnh: {
        type: String,
        message: 'o campo`{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'cnh',
        required: 'O campo `{PATH}` é obrigatório!',
        kind: 'String',
        path: 'cnh',
        valor: String
    },
    telefone: {
        type: String,
        trim: true,
        name: 'Telefone',
        path: 'telefone'
    },
    lat: {
        type: Number
      }
    ,
    lng: {
        type: Number
      },
    latAtual: {
        type: Number
      }
    ,
    lngAtual: {
        type: Number
      },
      token: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'descricao',
        kind: 'String',
        path: 'descricao',
        valor: String
    },
    token_organizacao: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'descricao',
        kind: 'String',
        path: 'descricao',
        valor: String
    }
});

module.exports = mongose.model('Motorista', motoristaSchema,'Motorista');