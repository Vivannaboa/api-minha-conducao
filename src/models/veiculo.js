var mongose = require('mongoose');
var Schema = mongose.Schema;

var TabelaPrecoSchema = new Schema({
    placa: {
        type: String,
        trim: true,
        name: 'Código',
        path: 'placa',
        minlength: [8, 'O valor do campo `{PATH}` (`{VALUE}`) não obedece  o tamanho minimo que é ({MINLENGTH}).']
    },
    modelo: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'modelo',
        required: 'O campo `{PATH}` é obrigatório!',
        kind: 'String',
        path: 'modelo',
        valor: String
    },
    capacidade: {
        type: Number,
        message: 'A `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'capacidade',
        required: 'O campo `{PATH}` é obrigatório!',
        kind: 'String',
        path: 'capacidade',
        valor: String
    }, 
    excluido: { type: Boolean, default: false },
    data_exclusao: {
        type: Date,
        trim: true,
        name: 'Data de exclusão',
        path: 'data_exclusao'
    },
    token_organizacao: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'modelo',
        kind: 'String',
        path: 'modelo',
        valor: String
    }
}, { timestamps: true });

module.exports = mongose.model('TabelaPreco', TabelaPrecoSchema,'TabelaPreco');