var mongose = require('mongoose');
var Schema = mongose.Schema;

var Sh = new Schema({
    id: String
    ,lat: {
        type: Number
      }
    ,lng: {
        type: Number
      }
});

var RoteiroShema = new Schema({
    descricao: {
        type: String,   
        message: 'O campo `{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'descricao',
        required: 'O campo `{PATH}` é obrigatório!',
        kind: 'String',
        path: 'descricao',
        minlength: [1, 'O valor do campo `{PATH}` (`{VALUE}`) não obedece  o tamanho minimo que é ({MINLENGTH}).'],
        valor: String
    },
    horaInicio: {
        type: String,
        trim: true,
        name: 'horaInicio',
        path: 'horaInicio',
        valor: Date
    },
    horaFim: {
        type: String,
        trim: true,
        name: 'horaFim',
        path: 'horaFim'
    },    
    latInicio: {
        type: Number
        
    },
    lngInicio: {
        type: Number
        
    },
    latFim: {
        type: Number
        
    }, 
    lngFim: {
        type: Number
        
    },     
    pass:[Sh]
},{ timestamps: true });

module.exports = mongose.model('Roteiro', RoteiroShema,'Roteiro');