var mongose = require('mongoose');
var jwt = require('jsonwebtoken');
var Schema = mongose.Schema;

var EmpresaSchema = new Schema({
    razao_social: {
        type: String,
        message: 'O campo`{PATH}` é um campo obrigatório!',
        trim: true,
        name: 'Razão social',        
        path: 'razao_social',
        required: true
    },
    cnpj: {
        type: String,
        trim: true,
        message: "O campo '{PATH}' é um campo obrigatório",
        unique: true,
        required: true,
        name: 'CPF/CNPJ',
        path: 'cpf_cnpj'
    },
    logradouro: {
        type: String,
        trim: true,
        name: 'Logradouro',
        path: 'logradouro'
    },
    numero: {
        type: Number,
        trim: true,
        name: 'Número',
        path: 'numero'
    },
    email: {
        type: String,
        trim: true,
        name: 'E-mail',
        path: 'email'
    },
    telefone: {
        type: String,
        trim: true,
        name: 'Telefone',
        path: 'telefone'
    },
    bairro: {
        type: String,
        trim: true,
        message: 'O `{PATH}` é um campo obrigatório!',
        required: true,
        name: "Bairro",
        path: 'bairro'
    },
    cidade: {
        type: String,
        message: 'A `{PATH}` é um campo obrigatório!',
        required: true,
        trim: true,
        name: "Cidade",
        path: 'cidade'
    },
    excluido: { type: Boolean },
    data_exclusao: {
        type: Date,
        trim: true,
        name: 'Data de exclusão',
        path: 'data_exclusao'
    },
    token_organizacao: String
    
}, { timestamps: true });

EmpresaSchema.methods.gerarToken = function (razao_social, cnpj) {
    return jwt.sign({ 'razao_social': razao_social, 'cnpj': cnpj }, 'segredomuitoForteEmpresa!');
}

module.exports = mongose.model('Empresa', EmpresaSchema,'Empresa');