var mongoose = require('mongoose');

var db_string = 'mongodb://localhost/api-minha-conducao';

mongoose.Promise = global.Promise;

mongoose.connect(db_string, {server: { socketOptions: { connectTimeoutMS: 2000 } } }, function(err, res) {
    if (err) {
        console.log('Não fooi possivel acesssar o banco');
    } else {
        console.log('Conectado ao banco de dados');
    }
});

