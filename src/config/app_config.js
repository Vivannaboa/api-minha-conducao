var express = require('express');

var app = module.exports = express();

var bodyParser = require('body-parser');

var allowCors = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '127.0.0.1:5000');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type', 'Authorization');
    res.header('Access-Control-Allow-Credentils', 'true');

    next();
}

let port = 5000
app.listen(port);

console.log('Rodando na porta: ' + port);

app.use(allowCors);

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: true
}));