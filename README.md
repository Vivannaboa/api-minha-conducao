# Api para sincronização do aplicativo Minha Condução

Api para sincronização de vendas, produtos, e clientes.

## Instalar Node.js

Link: https://nodejs.org/en/download/

## Instalar Git

Link: https://git-scm.com/downloads

## Clonar repositório
Primeiramente escolhe onde deseja salvar seu projeto, crie a pasta. 
Abra o powershell, e entre na pasta (use cd.. para ir para o diretório anterior e cd + diretório para entrar nele)

Após estar no diretório, execute o seguinte comando:
git clone https://gitlab.com/Vivannaboa/api-minha-conducao.git

cd api_minha_conducao

## Instalar Dependências

npm install

## A, aqui vamos usar o mongoDb

não sabe como configurar no windows

Leia isso: https://blog.ajduke.in/2013/04/10/install-setup-and-start-mongodb-on-windows/

## Dicas Mongo

https://github.com/Webschool-io/MongoDb-ebook

## Subir mudanças

git push -u origin master

## Build

-------------------------

<!--## Running unit tests

Run in CMD or Powershell `npm run test:unit` 

## Running integration tests

Run in CMD or Powershell `npm run test:integration` -->

## Running

Run in CMD or Powershell `npm start` 

